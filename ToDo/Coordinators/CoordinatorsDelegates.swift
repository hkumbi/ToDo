//
//  CoordinatorsDelegates.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-03-04.
//

import Foundation

protocol Coordinator : AnyObject {
    func start()
    func finish()
}

protocol ParentCoordinator : AnyObject {
    var childCoordinators : [Coordinator] { get }
    func childDidFinish(_ child : Coordinator)
}

protocol CoordinatorStartVCDelegate : AnyObject {
    func vcClosed()
}

protocol RootCoordinator {
    func start() -> MainNavigationController
}
