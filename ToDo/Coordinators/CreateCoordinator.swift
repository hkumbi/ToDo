//
//  CreateCoordinator.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-03-04.
//

import Foundation
import UIKit


class CreateCoordinator : Coordinator {
    
    var parentCoordinator : ParentCoordinator?

    private let navigation : MainNavigationController!
    private let initialVC : UIViewController?
    private let createService = CreateToDoService()
    
    
    init(_ navigation : MainNavigationController) {
        self.navigation = navigation
        initialVC = navigation.topViewController
    }
    
    func start() {
        
        let createVC = CreateViewController()
        createVC.coordinatorDelegate = self
        createVC.createService = createService
        navigation.pushViewController(createVC, animated: true)
    }
    
    func finish() {
        
        if let initialVC = initialVC {
            navigation.popToViewController(initialVC, animated: true)
        }
        
        parentCoordinator?.childDidFinish(self)
    }
    
}

extension CreateCoordinator : CreateVCCoordinatorDelegate {
    
    func saveToDo(_ vc : CreateViewController) {
        createService.createToDo()
    }
    
    func closeVC(_ vc : CreateViewController) {
        
        if userEnteredData(vc) {
            verifyCloseRequest()
        } else {
            finish()
        }
    }
    
    func addToDoStep(_ vc : CreateViewController) {

        let stepVC = CreateToDoStepViewController()
        stepVC.coordinatorDelegate = self
        stepVC.modalPresentationStyle = .overCurrentContext
        stepVC.createService = createService
        navigation.present(stepVC, animated: false)
    }
    
    func updateToDoStep(_ vc : CreateViewController, id : UUID) {
        
        let stepVC = CreateToDoStepViewController()
        stepVC.createService = createService
        stepVC.id = id
        stepVC.coordinatorDelegate = self
        stepVC.setUpExistingStep()
        stepVC.modalPresentationStyle = .overCurrentContext
        navigation.present(stepVC, animated: false)
    }
    
    private func verifyCloseRequest() {
        
        let alertVC = UIAlertController(
            title: "Loss Data",
            message: "By exiting you will lose all data for this ToDo. Are you sure you want to exit?",
            preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        let closeAction = UIAlertAction(title: "Close", style: .destructive){[unowned self]_ in
            finish()
        }
        
        alertVC.addAction(cancelAction)
        alertVC.addAction(closeAction)
        
        navigation.present(alertVC, animated: true)
    }
    
    private func userEnteredData(_ vc : CreateViewController) -> Bool {
        
        let scrollView = vc.contentView.scrollView
        let userEnteredText = scrollView.todoTextView.hasText || scrollView.notesTextView.hasText
        let stepExists = vc.dataSource.snapshot().numberOfItems != 0
        
        return userEnteredText || stepExists
    }
    
}

extension CreateCoordinator : CreateToDoStepVCCoordinatorDelegate {
    
    func closeVC(_ vc: CreateToDoStepViewController) {
        navigation.dismiss(animated: false)
    }
}
