//
//  HomeCoordinator.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-03-06.
//

import Foundation
import UIKit


class HomeCoordinator: RootCoordinator, ParentCoordinator {
    
    private var navigation : MainNavigationController!
    
    var childCoordinators: [Coordinator]
    
    
    init() {
        childCoordinators = []
    }
    
    func start() -> MainNavigationController {
        
        let vc = HomeViewController()
        
        navigation = MainNavigationController(rootViewController: vc)
        
        vc.controllerDelegate = self
        
        return navigation
    }
    
    func childDidFinish(_ child: Coordinator) {
        
        if let index =  childCoordinators.firstIndex(where: { child === $0 }) {
            childCoordinators.remove(at: index)
        }
    }
    
}


extension HomeCoordinator: HomeCoordinatorDelegate {
    
    func addToDo(_ vc: HomeViewController) {
        
        let createCoord = CreateCoordinator(navigation)
        
        childCoordinators.append(createCoord)
        
        createCoord.start()
    }
    
    func openToDo(_ vc: HomeViewController) {
        
        let vc = ToDoViewController()
        vc.coordinatorDelegate = self
        navigation.pushViewController(vc, animated: true)
    }
    
    func deleteAllToDos(_ vc: HomeViewController) {
        
        let alertVC = UIAlertController(
            title: "Delete All ToDos?",
            message: "Are you sure you want to delte all ToDos? You cannot undo this action.",
            preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        let deleteAction = UIAlertAction(title: "Delete", style: .destructive) { [unowned self]_ in
            deleteAllToDosHandler()
        }
        
        alertVC.addAction(cancelAction)
        alertVC.addAction(deleteAction)
        
        navigation.present(alertVC, animated: true)
    }
    
    private func deleteAllToDosHandler() {
        
    }
    
}


extension HomeCoordinator : ToDoCoordinatorDelegate {
    
    func exitVC(_ vc: ToDoViewController) {
        navigation.popViewController(animated: true)
    }
    
}
