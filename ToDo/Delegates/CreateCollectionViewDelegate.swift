//
//  CreateCollectionViewDelegate.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-02-28.
//

import Foundation


protocol CreateCollectionViewDelegate : AnyObject {
    func contentHeightChanged(_ height : CGFloat)
}
