//
//  CreateDelegates.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-03-03.
//

import Foundation

protocol CreateButtonDelegate : AnyObject {
    func addStepButtonPressed()
    func closeButtonPressed()
    func saveButtonPressed()
}

protocol CreateCellButtonDelegate : AnyObject {
    func stepButtonPressed(_ id : UUID)
    func deleteStepButtonPressed(_ id : UUID)
}

protocol CreateToDoStepButtonDelegate : AnyObject {
    func saveButtonPressed()
    func cancelButtonPressed()
}

