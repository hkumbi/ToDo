//
//  HomeDelegates.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-01-13.
//

import Foundation


protocol HomeButtonDelegate : AnyObject {
    func deleteButtonPressed()
    func addButtonPressed()
}

protocol ToDoCellDelgate : AnyObject {
    func completeButtonPressed(_ id : UUID)
    func todoButtonPressed(_ id : UUID)
}
