//
//  ToDoDelegates.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-01-27.
//

import Foundation


protocol ToDoDelegate : AnyObject {
    func backButtonPressed()
    func completeButtonPressed()
}

protocol StepDelegate : AnyObject {
    func stepCompleteButtonPressed(_ id : UUID)
}
