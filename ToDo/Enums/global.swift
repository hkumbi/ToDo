//
//  global.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-03-20.
//

import Foundation


enum CollectionChanged {
    case added, updated, deleted
}
