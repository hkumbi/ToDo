//
//  CustomFonts.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-01-09.
//

import UIKit

extension UIFont {
    
    static let titleFont = UIFont.systemFont(ofSize: 16, weight: .bold)
    
}
