//
//  IconImage.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-01-12.
//

import UIKit


extension UIImage {
    
    static func createIcon(name : String, weight : UIImage.SymbolWeight = .regular) -> UIImage? {
        
        UIImage(systemName: name)?.withConfiguration(UIImage.SymbolConfiguration(weight: weight))
    }
}
