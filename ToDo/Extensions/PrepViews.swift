//
//  PrepViews.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-01-09.
//

import Foundation
import UIKit


extension UIView {
    
    static func viewPreppedForAutoLayout() -> UIView {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
}


extension UILabel {
    
    static func preppedForAutoLayout() -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }
}


extension UIButton {
    
    static func preppedForAutoLayout() -> UIButton {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }
}


extension IconButton {
    
    static func iconPreppedForAutoLayout() -> IconButton {
        let icon = IconButton()
        icon.translatesAutoresizingMaskIntoConstraints = false
        return icon
    }
}

extension UICollectionView {
    
    static func collectionPreppedForAutoLayout() -> UICollectionView {
        let collection = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewLayout())
        collection.translatesAutoresizingMaskIntoConstraints = false
        return collection
    }
}

extension UIScrollView {
    
    static func preppedForAutoLayout() -> UIScrollView {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }
}

extension DefaultCollectionView {
    
    static func defaultPreppedForAutoLayout() -> DefaultCollectionView {
        let collection = DefaultCollectionView()
        collection.translatesAutoresizingMaskIntoConstraints = false
        return collection
    }
}

extension DefaultTableView {
    
    static func defaultPreppedForAutoLayout(_ style : UITableView.Style) -> DefaultTableView {
        let table = DefaultTableView(style)
        table.translatesAutoresizingMaskIntoConstraints = false
        return table
    }
}

extension UIImageView {
    
    static func preppedForAutoLayout() -> UIImageView {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }
}


extension UITextView {
    
    static func textPreppedForAutoLayout() -> UITextView {
        let textView = UITextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }
}

extension BackgroundButton {
    
    static func backgroundPreppedForAutoLayout() -> BackgroundButton {
        let background = BackgroundButton()
        background.translatesAutoresizingMaskIntoConstraints = false
        return background
    }
}
