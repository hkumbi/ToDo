//
//  ReturnHighest.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-03-12.
//

import Foundation


extension CGFloat {
    
    func returnHighest(_ value : CGFloat) -> CGFloat {
        self >= value ? self : value
    }
    
}
