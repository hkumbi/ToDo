//
//  ScreenSize.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-01-09.
//

import UIKit

extension UIScreen {
    
    static var width : CGFloat { size.width }
    static var height : CGFloat { size.height }
    
    static var size : CGSize = .zero
}
