//
//  ThemeColors.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-01-09.
//

import UIKit


extension UIColor {
    
    static let customPurple = UIColor(red: 143/255, green: 57/255, blue: 133/257, alpha: 1)
    
}
