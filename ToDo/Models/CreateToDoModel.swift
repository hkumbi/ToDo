//
//  CreateToDoModel.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-03-18.
//

import Foundation


class CreateToDoModel {
    
    let id = UUID()
    
    var todo : String {
        get {todoSource }
    }
    
    var notes : String {
        get { notesSource }
    }
    
    var steps : [StepModel] {
        get { stepsSource }
    }
    
    private var todoSource : String = ""
    private var notesSource : String = ""
    private var stepsSource : [StepModel] = []
    
    init(todo: String, notes: String, steps: [StepModel]) {
        self.todoSource = todo
        self.notesSource = notes
        self.stepsSource = steps
    }
    
}
