//
//  StepModel.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-03-11.
//

import Foundation


class StepModel: Hashable, Equatable {
    
    static func == (lhs: StepModel, rhs: StepModel) -> Bool {
        lhs.id == rhs.id
    }
    
    static func fakeData(_ numOfItems : Int) -> [StepModel] {
        
        var items : [StepModel] = []
        
        for _ in 1 ..< numOfItems {
            items.append(StepModel("Hello There"))
        }
        
        return items
    }
    
    let id = UUID()
    var text : String = ""
    
    
    init(_ text: String) {
        self.text = text
    }
    
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
        hasher.combine(text)
    }
    
}
