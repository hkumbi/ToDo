//
//  ToDoCollectionModels.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-01-26.
//

import Foundation


class ToDoCollectionBaseModel : Hashable, Equatable {
    
    static func == (lhs: ToDoCollectionBaseModel, rhs: ToDoCollectionBaseModel) -> Bool {
        lhs.id == rhs.id
    }
    
    let id = UUID()
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
}

class ToDoInfoModel: ToDoCollectionBaseModel {
    
    static func fakeData() -> ToDoInfoModel {
        
        let info = ToDoInfoModel()
        info.title = "Hello there"
        info.notes = "This is the notes section hello there. That I've written as a joke to simpky test the workings of my view lets go"
        return info
    }
    
    var title : String = ""
    var notes : String = ""

    override func hash(into hasher: inout Hasher) {
        super.hash(into: &hasher)
        
        hasher.combine(title)
        hasher.combine(notes)
    }
    
}

class ToDoStepsModel: ToDoCollectionBaseModel {
    
    static func fakeData() -> ToDoStepsModel {
        
        let steps = ToDoStepsModel()
        steps.isComplete = false
        steps.position = 2
        steps.stepText = "Hello there this is a step. Hello there this is a step. Hello there this is a step. Hello there this is a step. Hello there this is a step."
        return steps
    }
    
    var isComplete : Bool = false
    var position : Int = 1
    var stepText : String = ""
    
    override func hash(into hasher: inout Hasher) {
        super.hash(into: &hasher)
        
        hasher.combine(isComplete)
        hasher.combine(position)
        hasher.combine(stepText)
    }
    
}
