//
//  ToDoModel.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-01-13.
//

import Foundation


class ToDo : Hashable, Equatable {
    
    static func == (lhs: ToDo, rhs: ToDo) -> Bool {
        lhs.id == rhs.id
    }
        
    let id : UUID
    let title : String
    let todo : String
    var isCompleted : Bool
    
    init(id: UUID, title: String = "", todo: String = "", isCompleted: Bool = false) {
        self.id = id
        self.title = title
        self.todo = todo
        self.isCompleted = isCompleted
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
        hasher.combine(title)
        hasher.combine(todo)
        hasher.combine(isCompleted)
    }
    
}
