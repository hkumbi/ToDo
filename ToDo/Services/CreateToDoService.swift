//
//  CreateToDoService.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-03-17.
//

import Foundation


class CreateToDoService {
        
    struct Observation {
        weak var observer : CreateToDoObserver?
    }
    
    var observers = [ObjectIdentifier : Observation]()
    
    var todo : String {
        get { todoSource }
    }
    
    var notes : String {
        get { notesSource }
    }
    
    var steps : [StepModel] {
        get { stepsSource }
    }
    
    private var todoSource : String = ""
    private var notesSource : String = ""
    private var stepsSource : [StepModel] = []
    
    
    init() {

    }
    
    deinit {
        print("Deinit create service")
    }
    
    func addObserver(_ obs : CreateToDoObserver) {
        let id = ObjectIdentifier(obs)
        observers[id] = Observation(observer: obs)
    }
    
    func removeObserver(_ obs : CreateToDoObserver) {
        let id = ObjectIdentifier(obs)
        observers.removeValue(forKey: id)
    }
    
    func createToDo() -> CreateToDoModel {
        
        CreateToDoModel(todo: todoSource, notes: notesSource, steps: stepsSource)
    }
    
}

extension CreateToDoService {
    
    func updateTodoText(_ text : String) {
        // Do any validation you want
        todoSource = text
        todoTextDidChange()
    }
    
    func updateNotesText(_ text : String) {
        // Do any validation you want
        notesSource = text
        notesTextDidChange()
    }
    
    func insertStep(id : UUID?, _ text : String) {
        
        if let id = id, let step = stepsSource.first(where: { $0.id == id }) {
            step.text = text
            stepsDidChange(change: .updated, itemsAffected: [id])
        } else {
            let newStep = StepModel(text)
            stepsSource.append(newStep)
            stepsDidChange(change: .added, itemsAffected: [newStep.id])
        }
    }
    
    func removeStep(_ step : StepModel) {
        if let index = steps.firstIndex(of: step) {
            stepsSource.remove(at: index)
            stepsDidChange(change: .deleted, itemsAffected: [step.id])
        }
    }
    
}

extension CreateToDoService {
    
    private func todoTextDidChange() {
        
        for (id, observation) in observers {
            
            guard let observer = observation.observer else {
                observers.removeValue(forKey: id)
                continue
            }
            
            observer.todoTextChanged(self)
        }
    }
    
    private func notesTextDidChange() {
        
        for (id, observation) in observers {
            
            guard let observer = observation.observer else {
                observers.removeValue(forKey: id)
                continue
            }
            
            observer.notesTextChanged(self)
        }
    }
    
    private func stepsDidChange(change : CollectionChanged, itemsAffected : [UUID]) {
        
        for (id, observation) in observers {
            
            guard let observer = observation.observer else {
                observers.removeValue(forKey: id)
                continue
            }
            
            observer.stepsChanged(self, change: change, itemsAffected: itemsAffected)
        }
    }
    
}

protocol CreateToDoObserver : AnyObject {
    
    func todoTextChanged(_ service : CreateToDoService)
    func notesTextChanged(_ service : CreateToDoService)
    func stepsChanged(_ service : CreateToDoService, change : CollectionChanged, itemsAffected : [UUID])
}
