//
//  CreateToDoStepViewController.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-03-01.
//

import UIKit

class CreateToDoStepViewController: UIViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle { .darkContent }
    
    unowned var coordinatorDelegate : CreateToDoStepVCCoordinatorDelegate!

    let contentView = CreateToDoStepView()
    var createService : CreateToDoService?
    var id : UUID? {
        didSet { isStepNewOrOld() }
    }
    
    private let animationDuration : CGFloat = 0.2
        

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.addSubview(contentView)
        
        contentView.stepTextView.delegate = self
        contentView.buttonDelegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        isStepNewOrOld()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        contentView.bottomSafeArea = view.safeAreaInsets.bottom
        contentView.modalBottomConstraint.constant = contentView.height
        openModal()
    }
    
    func setUpExistingStep() {
        
        guard let id = id else { return }

        if let step = createService?.steps.first(where: { $0.id == id }) {
            contentView.stepTextView.text = step.text
            contentView.stepTextView.becomeFirstResponder()
        }
    }
    
    private func isStepNewOrOld() {
        contentView.titleLabel.text = id == nil ? "Add new step" : "Edit Step"
    }

    private func openModal() {
        contentView.stepTextView.becomeFirstResponder()
        
        UIView.animate(withDuration: animationDuration, delay: 0) {
            [unowned self] in
            
            contentView.modalBottomConstraint.constant = 0
            contentView.dimmingView.alpha = CreateToDoStepView.maxDimming
            view.layoutIfNeeded()
        }
    }
    
    private func closeModal() {
        
        UIView.animate(withDuration: animationDuration, delay: 0) {
            [unowned self] in
            
            contentView.modalBottomConstraint.constant = contentView.height
            contentView.dimmingView.alpha = 0
            view.layoutIfNeeded()
        } completion: { [unowned self]_ in
            coordinatorDelegate.closeVC(self)
        }
    }
    
    @objc private func keyboardWillHide(_ notification: NSNotification) {

        guard let userInfo = notification.userInfo else { return }

        let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! CGFloat
        let curveRaw = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as! UInt
        let option = UIView.AnimationOptions(rawValue: curveRaw)

        UIView.animate(withDuration: duration, delay: 0, options: option){
            [unowned self] in
            
            contentView.modalBottomConstraint.constant = 0
            view.layoutIfNeeded()
        }
    }
    
    @objc private func keyboardWillShow(_ notification: NSNotification) {

        guard let userInfo = notification.userInfo else { return }

        let keyboardFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
        let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! CGFloat
        let curveRaw = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as! UInt
        let option = UIView.AnimationOptions(rawValue: curveRaw)
        let constant = -keyboardFrame.height + view.safeAreaInsets.bottom
        
        UIView.animate(withDuration: duration, delay: 0, options: option){
            [unowned self] in
            
            contentView.modalBottomConstraint.constant = constant
            view.layoutIfNeeded()
        }
    }
    
}

extension CreateToDoStepViewController : UITextViewDelegate {
    
}

extension CreateToDoStepViewController : CreateToDoStepButtonDelegate {
    
    func saveButtonPressed() {
        createService?.insertStep(id: id, contentView.stepTextView.text)
        contentView.stepTextView.resignFirstResponder()
        closeModal()
    }
    
    func cancelButtonPressed() {
        contentView.stepTextView.resignFirstResponder()
        closeModal()
    }
}

// Coordinator Delegate
protocol CreateToDoStepVCCoordinatorDelegate : AnyObject {
    func closeVC(_ vc : CreateToDoStepViewController)
}
