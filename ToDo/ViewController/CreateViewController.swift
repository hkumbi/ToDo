//
//  CreateViewController.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-01-27.
//

import UIKit

class CreateViewController: UIViewController {
    
    typealias DataSource = UICollectionViewDiffableDataSource<String, StepModel>
    typealias Snap = NSDiffableDataSourceSnapshot<String, StepModel>
    
    override var preferredStatusBarStyle: UIStatusBarStyle { .darkContent }
    
    unowned var coordinatorDelegate : CreateVCCoordinatorDelegate!
    
    let contentView = CreateToDoView()
    lazy var dataSource = makeDataSource()
    var createService : CreateToDoService? {
        didSet { setUpCreateService() }
    }
    
    private let sections = ["Main"]
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = .white
        view.addSubview(contentView)
        
        contentView.buttonDelegate = self
        contentView.scrollView.buttonDelegate = self
        contentView.scrollView.todoTextView.delegate = self
        contentView.scrollView.notesTextView.delegate = self
        
        configureCollectionView()
    }
    
    private func makeDataSource() -> DataSource {
        
        DataSource(collectionView: contentView.scrollView.stepsCollectionView){
            [unowned self]collectionView, indexPath, item in
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CreateStepCollectionViewCell.id, for: indexPath) as? CreateStepCollectionViewCell
            configureCell(cell, indexPath.row, item)
            return cell
        }
    }
    
    private func configureCell(_ cell : CreateStepCollectionViewCell?, _ index: Int, _ item : StepModel) {
        cell?.buttonDelegate = self
        cell?.id = item.id
        cell?.positionLabel.text = "\(index+1)"
        cell?.stepLabel.text = item.text
    }
    
    private func configureCollectionView() {
        let collection = contentView.scrollView.stepsCollectionView
        collection.dataSource = dataSource
        collection.delegate = self
        collection.collectionViewLayout = configureCollectionLayout()
        collection.register(CreateStepCollectionViewCell.self, forCellWithReuseIdentifier: CreateStepCollectionViewCell.id)
    }
    
    private func configureCollectionLayout() -> UICollectionViewFlowLayout {
        let flow = UICollectionViewFlowLayout()
        flow.minimumInteritemSpacing = 10
        flow.scrollDirection = .vertical
        return flow
    }
    
    private func setUpCreateService() {
        
        guard let createService = createService else { return }
        
        createService.addObserver(self)
        
        var snap = Snap()
        snap.appendSections(sections)
        snap.appendItems(createService.steps)
        dataSource.apply(snap)
    }

}

extension CreateViewController : UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let item = dataSource.itemIdentifier(for: indexPath)
        
        if let item = item {
            return CGSize(width: UIScreen.width, height: CreateStepCollectionViewCell.calculateHeight(item.text))
        } else {
            return .zero
        }
    }
}

extension CreateViewController : UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        
        if contentView.scrollView.todoTextView == textView {
            contentView.scrollView.todoPlaceholderLabel.isHidden = !textView.text.isEmpty
        } else {
            contentView.scrollView.notesPlaceholderLabel.isHidden = !textView.text.isEmpty
        }
    }
}

extension CreateViewController : CreateButtonDelegate {
    
    func closeButtonPressed() {
        coordinatorDelegate.closeVC(self)
    }
    
    func saveButtonPressed() {
        createService?.updateTodoText(contentView.scrollView.todoTextView.text)
        createService?.updateNotesText(contentView.scrollView.notesTextView.text)
        coordinatorDelegate.saveToDo(self)
    }
    
    func addStepButtonPressed() {
        coordinatorDelegate.addToDoStep(self)
    }
    
}

extension CreateViewController : CreateCellButtonDelegate {
    
    func deleteStepButtonPressed(_ id: UUID) {
        
        let item = dataSource.snapshot().itemIdentifiers.first(where: { $0.id == id })
        
        if let item = item {
            createService?.removeStep(item)
        }
    }
    
    func stepButtonPressed(_ id: UUID) {
        coordinatorDelegate.updateToDoStep(self, id: id)
    }
}

extension CreateViewController : CreateToDoObserver {
    
    func todoTextChanged(_ service: CreateToDoService) {
        
    }
    
    func notesTextChanged(_ service: CreateToDoService) {
        
    }
    
    func stepsChanged(_ service: CreateToDoService, change: CollectionChanged, itemsAffected: [UUID]) {

        switch change {
        case .added:
            var snap = dataSource.snapshot()
            let items = service.steps.filter({ step in itemsAffected.contains(where: { step.id == $0 }) })
            snap.appendItems(items)
            dataSource.apply(snap, animatingDifferences: true)

        case .updated:
            var snap = dataSource.snapshot()
            let items = service.steps.filter({ step in itemsAffected.contains(where: { step.id == $0 }) })
            snap.reloadItems(items)
            dataSource.apply(snap)

        case .deleted:
            var snap = dataSource.snapshot()
            let items = snap.itemIdentifiers.filter({ step in itemsAffected.contains(where: { step.id == $0 }) })
            snap.deleteItems(items)
            dataSource.apply(snap, animatingDifferences: false)
            
            var secondSnap = dataSource.snapshot()
            secondSnap.reconfigureItems(secondSnap.itemIdentifiers)
            dataSource.apply(secondSnap)
        }
    }
    
}


// Coordinator Delegate
protocol CreateVCCoordinatorDelegate : AnyObject {
    func saveToDo(_ vc : CreateViewController)
    func closeVC(_ vc : CreateViewController)
    func addToDoStep(_ vc : CreateViewController)
    func updateToDoStep(_ vc : CreateViewController, id : UUID)
}
