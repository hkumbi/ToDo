//
//  HomeViewController.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-01-09.
//

import UIKit

class HomeViewController: UIViewController {
    
    private typealias DataSource = UITableViewDiffableDataSource<String, ToDo>
    private typealias Snap = NSDiffableDataSourceSnapshot<String, ToDo>
    
    override var preferredStatusBarStyle: UIStatusBarStyle { .darkContent }
        
    unowned var controllerDelegate : HomeCoordinatorDelegate!

    private let contentView = HomeView()
    private lazy var dataSource = setUpDataSource()
    private let cellID = "ToDoCell"
    private let mainSection = "MainSection"
        

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = .white
        view.addSubview(contentView)
        
        contentView.buttonDelegate = self
        
        setUpTableView()
        setUpTableViewLayout()
        fakeData()
    }
    
    private func setUpDataSource() -> DataSource {
        
        return DataSource(tableView: contentView.tableView){
            [unowned self]tableView, indexPath, item in
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
            configureCell(cell as! ToDoTableViewCell, item: item)
            return cell
        }
    }

}

extension HomeViewController {
    
    private func setUpTableView() {
        
        let sideInset = UIScreen.width*0.1
        
        contentView.tableView.delegate = self
        contentView.tableView.register(ToDoTableViewCell.self, forCellReuseIdentifier: cellID)
        contentView.tableView.allowsSelection = false
        contentView.tableView.separatorStyle = .singleLine
        contentView.tableView.separatorInset = .init(top: 0, left: sideInset, bottom: 0, right: sideInset)
    }
    
    private func setUpTableViewLayout() {
        contentView.tableView.rowHeight = ToDoTableViewCell.height
    }
    
    private func configureCell(_ cell : ToDoTableViewCell, item : ToDo) {
        
        cell.buttonDelegate = self
        cell.id = item.id
        cell.title = "Hello"
        cell.todo = "Hello"
        cell.isCompleted = item.isCompleted
    }
    
    private func fakeData() {
        
        var snap = Snap()
        
        snap.appendSections([mainSection])
        
        for _ in 0..<5 {
            snap.appendItems([ToDo(id: UUID(), title: "Hell this is from a data model")])
        }
        
        dataSource.apply(snap)
    }
    
}


extension HomeViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let deleteAction = UIContextualAction(style: .destructive, title: nil){
            [unowned self]_,_,completionHandler in
            
            if deleteToDo(indexPath) {
                completionHandler(true)
            } else {
                completionHandler(false)
            }
        }
        
        deleteAction.image = UIImage(systemName: "trash")?.withTintColor(.black)
        deleteAction.backgroundColor = .red

        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    
    private func deleteToDo(_ indexPath : IndexPath) -> Bool {
        
        var snap = dataSource.snapshot()
        
        if let item = dataSource.itemIdentifier(for: indexPath){
            snap.deleteItems([item])
            dataSource.apply(snap)
            return true
        }
        
        return false
    }
}


extension HomeViewController : ToDoCellDelgate {
    
    func todoButtonPressed(_ id: UUID) {

        if let (item, _) = getToDoItemAndCell(id) {
            navigateToToDoModal(item)
        }
    }
    
    func completeButtonPressed(_ id: UUID) {

        if let (item, cell) = getToDoItemAndCell(id) {
            changeToDoCompletionStatus(item: item, cell: cell)
        }
    }
    
    private func getToDoItemAndCell(_ id : UUID) -> (ToDo, ToDoTableViewCell?)? {
        // Tuple is nullable since the todo might not exist
        // Cell is nullabe because it might not have been allocated
        
        let item = dataSource.snapshot().itemIdentifiers.first(where: { $0.id == id })
        
        if let item = item, let indexPath = dataSource.indexPath(for: item) {
            
            let cell = contentView.tableView.cellForRow(at: indexPath) as? ToDoTableViewCell
            
            return (item, cell)
        }
        
        return nil
    }
    
    private func navigateToToDoModal(_ item : ToDo){
        controllerDelegate.openToDo(self)
    }
    
    private func changeToDoCompletionStatus(item : ToDo, cell : ToDoTableViewCell?) {
        
        item.isCompleted = !item.isCompleted
        cell?.isCompleted = item.isCompleted
    }
}


extension HomeViewController : HomeButtonDelegate {
    
    func deleteButtonPressed() {
        controllerDelegate.deleteAllToDos(self)
    }
    
    func addButtonPressed() {
        controllerDelegate.addToDo(self)
    }
}


// Coordinator Delegate
protocol HomeCoordinatorDelegate : AnyObject {
    func addToDo(_ vc : HomeViewController)
    func openToDo(_ vc : HomeViewController)
    func deleteAllToDos(_ vc : HomeViewController)
}
