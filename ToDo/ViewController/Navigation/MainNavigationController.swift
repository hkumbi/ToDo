//
//  MainNavigationController.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-03-05.
//

import UIKit

class MainNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Dont show a navigation bar
        setNavigationBarHidden(true, animated: false)

    }
    

    

}
