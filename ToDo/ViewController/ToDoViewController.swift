//
//  ToDoViewController.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-01-17.
//

import UIKit

class ToDoViewController: UIViewController {
    
    private typealias DataSource = UICollectionViewDiffableDataSource<String, ToDoCollectionBaseModel>
    private typealias Snap = NSDiffableDataSourceSnapshot<String, ToDoCollectionBaseModel>
    
    override var preferredStatusBarStyle: UIStatusBarStyle { .darkContent }
    
    unowned var coordinatorDelegate : ToDoCoordinatorDelegate!
    
    private let contentView = ToDoView()
    private let infoSection = "InfoSection"
    private let stepsSection = "StepsSection"
    private lazy var dataSource = makeDataSource()
    private var isComplete : Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = .white
        view.addSubview(contentView)
        
        setUpViews()
        setUpCollectionView()
        setUpCollectionViewLayout()
        fakeData()
    }
    
    private func makeDataSource() -> DataSource {
        
        let dataSource = DataSource(collectionView: contentView.collectionView){
            [unowned self]collection, indexPath, item in
            
            if let item = item as? ToDoInfoModel {
                return configureInfoCell(collection, indexPath: indexPath, item: item)
            } else if let item = item as? ToDoStepsModel {
                return configureStepsCell(collection, indexPath: indexPath, item: item)
            } else {
                fatalError("Item is not configured for a cell")
            }
        }
        
        dataSource.supplementaryViewProvider = {
            collection, kind, indexPath in
            
            collection.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: StepHeaderCollectionReusableView.id, for: indexPath)
        }
        
        return dataSource
    }
    
    private func configureInfoCell(_ collection : UICollectionView, indexPath : IndexPath, item : ToDoInfoModel) -> UICollectionViewCell? {
        
        let cell = collection.dequeueReusableCell(withReuseIdentifier: ToDoInfoCollectionViewCell.id, for: indexPath) as? ToDoInfoCollectionViewCell
        cell?.title = item.title
        cell?.notes = item.notes
        return cell
    }
    
    private func configureStepsCell(_ collection : UICollectionView, indexPath : IndexPath, item : ToDoStepsModel) -> UICollectionViewCell? {
        
        let cell = collection.dequeueReusableCell(withReuseIdentifier: StepsCollectionViewCell.id, for: indexPath) as? StepsCollectionViewCell
        cell?.isComplete = item.isComplete
        cell?.stepCount = "\(item.position)."
        cell?.step = item.stepText
        cell?.stepDelegate = self
        cell?.id = item.id
        return cell
    }

}


extension ToDoViewController {
    
    private func setUpViews() {
        contentView.todoDelegate = self
        contentView.isComplete = isComplete
    }
    
    private func setUpCollectionView() {
        contentView.collectionView.delegate = self
        contentView.collectionView.dataSource = dataSource
        contentView.collectionView.register(ToDoInfoCollectionViewCell.self, forCellWithReuseIdentifier: ToDoInfoCollectionViewCell.id)
        contentView.collectionView.register(StepsCollectionViewCell.self, forCellWithReuseIdentifier: StepsCollectionViewCell.id)
        contentView.collectionView.register(StepHeaderCollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: StepHeaderCollectionReusableView.id)
    }
    
    private func setUpCollectionViewLayout() {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.minimumLineSpacing = 0
        flowLayout.minimumInteritemSpacing = 0
        contentView.collectionView.collectionViewLayout = flowLayout
    }
    
    private func fakeData() {
        var snap = Snap()
        snap.appendSections([infoSection, stepsSection])
        snap.appendItems([ToDoInfoModel.fakeData()], toSection: infoSection)
        snap.appendItems([ToDoStepsModel.fakeData(), ToDoStepsModel.fakeData(), ToDoStepsModel.fakeData(), ToDoStepsModel.fakeData(), ToDoStepsModel.fakeData(), ToDoStepsModel.fakeData(), ToDoStepsModel.fakeData()], toSection: stepsSection)
        dataSource.apply(snap)
    }
    
}


extension ToDoViewController : UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let item = dataSource.itemIdentifier(for: indexPath)
        
        if let item = item as? ToDoInfoModel {
            return ToDoInfoCollectionViewCell.calculateSize(title: item.title, notes: item.notes)
        } else if let item = item as? ToDoStepsModel {
            return StepsCollectionViewCell.calculateSize(item.stepText)
        } else {
            return .zero
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        if section == 1 {
            return StepHeaderCollectionReusableView.size
        } else {
            return .zero
        }
    }
    
}


extension ToDoViewController : ToDoDelegate {
    
    func backButtonPressed() {
        coordinatorDelegate.exitVC(self)
    }
    
    func completeButtonPressed() {
        isComplete = !isComplete
        contentView.isComplete = isComplete
    }
}

extension ToDoViewController : StepDelegate {
    
    func stepCompleteButtonPressed(_ id: UUID) {
        
        if let item = dataSource.snapshot().itemIdentifiers.first(where: { $0.id == id }) as? ToDoStepsModel, let indexPath = dataSource.indexPath(for: item) {
            
            item.isComplete = !item.isComplete
            let cell = contentView.collectionView.cellForItem(at: indexPath) as? StepsCollectionViewCell
            cell?.isComplete = item.isComplete
        }
        
    }
}


// Coordinator Delegate
protocol ToDoCoordinatorDelegate : AnyObject {
    func exitVC(_ vc : ToDoViewController)
}
