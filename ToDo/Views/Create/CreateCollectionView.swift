//
//  CreateCollectionView.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-02-28.
//

import UIKit

class CreateCollectionView: UICollectionView {

    override var contentSize: CGSize {
        didSet { createDelegate.contentHeightChanged(contentSize.height) }
    }
    
    unowned var createDelegate : CreateCollectionViewDelegate!

    init() {
        super.init(frame: .zero, collectionViewLayout: UICollectionViewLayout())
        
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
}
