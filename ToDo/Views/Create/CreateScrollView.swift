//
//  CreateScrollView.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-02-27.
//

import UIKit

class CreateScrollView: UIScrollView {

    let contentView : UIView = .viewPreppedForAutoLayout()
    let todoTextView : UITextView = .textPreppedForAutoLayout()
    let todoPlaceholderLabel = UILabel()
    let notesTextView : UITextView = .textPreppedForAutoLayout()
    let notesPlaceholderLabel = UILabel()
    let stepsLineView : UIView = .viewPreppedForAutoLayout()
    let stepsTitleLabel : UILabel = .preppedForAutoLayout()
    let stepsCollectionView = CreateCollectionView()
    let stepsAddButton : BackgroundButton = .backgroundPreppedForAutoLayout()
    
    unowned var buttonDelegate : CreateButtonDelegate!
    
    private var collectionHeightConstraint : NSLayoutConstraint!
    private let titleFont = UIFont.systemFont(ofSize: 22, weight: .semibold)
    private let todoFont = UIFont.systemFont(ofSize: 18)
    
    
    init() {
        super.init(frame: .zero)
        
        configureViews()
        setUpGestures()
        createPlaceholders()
        
        self.addSubview(contentView)
         
        self.contentView.addSubview(todoTextView)
        self.contentView.addSubview(notesTextView)
        self.contentView.addSubview(stepsLineView)
        self.contentView.addSubview(stepsTitleLabel)
        self.contentView.addSubview(stepsCollectionView)
        self.contentView.addSubview(stepsAddButton)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        
        alwaysBounceVertical = true
        backgroundColor = .white
        contentView.backgroundColor = .white
        
        todoTextView.isScrollEnabled = false
        todoTextView.font = titleFont
        todoTextView.textContainerInset = .init(top: 10, left: 10, bottom: 10, right: 10)

        notesTextView.isScrollEnabled = false
        notesTextView.font = todoFont
        notesTextView.textContainerInset = .init(top: 10, left: 10, bottom: 10, right: 10)
        
        stepsLineView.backgroundColor = .lightGray.withAlphaComponent(0.7)
                
        stepsTitleLabel.text = "Steps"
        stepsTitleLabel.textColor = .black
        stepsTitleLabel.font = .systemFont(ofSize: 20, weight: .semibold)
        
        stepsCollectionView.createDelegate = self
        
        stepsAddButton.setTitle("Add", for: .normal)
        stepsAddButton.setTitleColor(.white, for: .normal)
        stepsAddButton.titleLabel?.font = .systemFont(ofSize: 16, weight: .bold)
        stepsAddButton.layer.cornerRadius = 20
        stepsAddButton.baseColor = .customPurple
        stepsAddButton.highlightedColor = .customPurple.withAlphaComponent(0.7)
    }
    
    private func setUpGestures() {
        stepsAddButton.addTarget(self, action: #selector(stepsAddButtonPressed), for: .touchUpInside)
    }
    
    private func createPlaceholders() {
        
        todoPlaceholderLabel.text = "Your ToDo . . ."
        todoPlaceholderLabel.textColor = .darkGray
        todoPlaceholderLabel.font = titleFont
        todoPlaceholderLabel.sizeToFit()
        todoTextView.addSubview(todoPlaceholderLabel)
        todoPlaceholderLabel.frame.origin = CGPoint(x: todoTextView.textContainerInset.left+5, y: (titleFont.pointSize/2))
        
        notesPlaceholderLabel.text = "Notes . . ."
        notesPlaceholderLabel.textColor = .darkGray
        notesPlaceholderLabel.font = todoFont
        notesPlaceholderLabel.sizeToFit()
        notesTextView.addSubview(notesPlaceholderLabel)
        notesPlaceholderLabel.frame.origin = CGPoint(x: notesTextView.textContainerInset.left+5, y: (todoFont.pointSize/2))
    }
    
    private func makeConstraints() {
                
        translatesAutoresizingMaskIntoConstraints = false
        collectionHeightConstraint = stepsCollectionView.heightAnchor.constraint(equalToConstant: 0)
                
        NSLayoutConstraint.activate([
            contentView.topAnchor.constraint(equalTo: contentLayoutGuide.topAnchor),
            contentView.leftAnchor.constraint(equalTo: frameLayoutGuide.leftAnchor),
            contentView.rightAnchor.constraint(equalTo: frameLayoutGuide.rightAnchor),
            contentView.bottomAnchor.constraint(equalTo: contentLayoutGuide.bottomAnchor),
            
            contentView.centerXAnchor.constraint(equalTo: contentLayoutGuide.centerXAnchor),
            contentView.centerYAnchor.constraint(equalTo: contentLayoutGuide.centerYAnchor),
            
            todoTextView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            todoTextView.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 0.9),
            todoTextView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            
            notesTextView.topAnchor.constraint(equalTo: todoTextView.bottomAnchor, constant: 20),
            notesTextView.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 0.9),
            notesTextView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            
            stepsLineView.topAnchor.constraint(equalTo: notesTextView.bottomAnchor, constant: 20),
            stepsLineView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.9),
            stepsLineView.centerXAnchor.constraint(equalTo: centerXAnchor),
            stepsLineView.heightAnchor.constraint(equalToConstant: 1),
            
            stepsTitleLabel.topAnchor.constraint(equalTo: stepsLineView.bottomAnchor, constant: 20),
            stepsTitleLabel.leftAnchor.constraint(equalTo: todoTextView.leftAnchor, constant: 15),
            
            stepsCollectionView.topAnchor.constraint(equalTo: stepsTitleLabel.bottomAnchor, constant: 10),
            stepsCollectionView.widthAnchor.constraint(equalTo: contentView.widthAnchor),
            stepsCollectionView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            collectionHeightConstraint,
                        
            stepsAddButton.topAnchor.constraint(equalTo: stepsCollectionView.bottomAnchor, constant: 20),
            stepsAddButton.widthAnchor.constraint(equalToConstant: 100),
            stepsAddButton.heightAnchor.constraint(equalToConstant: 40),
            stepsAddButton.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            stepsAddButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
        ])
    }

    @objc private func stepsAddButtonPressed() {
        buttonDelegate.addStepButtonPressed()
    }
    
}


extension CreateScrollView : CreateCollectionViewDelegate {
    
    func contentHeightChanged(_ height: CGFloat) {
        collectionHeightConstraint.constant = height
    }
}
