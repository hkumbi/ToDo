//
//  CreateStepCollectionViewCell.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-03-12.
//

import UIKit

class CreateStepCollectionViewCell: UICollectionViewCell {
    
    static let id = "CreateStepCollectionViewCell"
    
    private static let spacing : CGFloat = 10
    private static let buttonSize : CGFloat = 25
    private static let stepLabelWidth = UIScreen.width*0.65
    private static let stepFont = UIFont.systemFont(ofSize: 16)
    
    static func calculateHeight(_ text : String) -> CGFloat {
        
        let minimumHeight = spacing + buttonSize
        let textHeight = String.getLabelHeight(text: text, font: stepFont, width: stepLabelWidth) + spacing

        return minimumHeight.returnHighest(textHeight) + 1
    }
    
    let stepButton : UIButton = .preppedForAutoLayout()
    let positionLabel : UILabel = .preppedForAutoLayout()
    let stepLabel : UILabel = .preppedForAutoLayout()
    let deleteButton : IconButton = .iconPreppedForAutoLayout()
    
    var id : UUID!
    unowned var buttonDelegate : CreateCellButtonDelegate!
    
    private let spacing : CGFloat = 10
    private let buttonHeight : CGFloat = 20
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureViews()
        setUpGestures()
        
        self.contentView.addSubview(stepButton)
        self.contentView.addSubview(positionLabel)
        self.contentView.addSubview(stepLabel)
        self.contentView.addSubview(deleteButton)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        backgroundColor = .white
        
        positionLabel.font = .systemFont(ofSize: 14, weight: .medium)
        positionLabel.textColor = .darkGray
        positionLabel.textAlignment = .center
        
        stepLabel.font = Self.stepFont
        stepLabel.textColor = .black
        stepLabel.textAlignment = .left
        stepLabel.numberOfLines = 0
        
        deleteButton.setImage(.createIcon(name: "xmark.circle"), for: .normal)
        deleteButton.setIconColor(.red)
        deleteButton.iconContentMode = .scaleAspectFit
    }
    
    private func setUpGestures() {
        stepButton.addTarget(self, action: #selector(stepButtonPressed), for: .touchUpInside)
        deleteButton.addTarget(self, action: #selector(deleteButtonPressed), for: .touchUpInside)
    }
    
    private func makeConstraints() {
        
        deleteButton.pinImageView()
        
        NSLayoutConstraint.activate([
            positionLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Self.spacing/2),
            positionLabel.rightAnchor.constraint(equalTo: stepLabel.leftAnchor, constant: -10),
            positionLabel.widthAnchor.constraint(equalToConstant: 40),
            
            stepLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Self.spacing/2),
            stepLabel.widthAnchor.constraint(equalToConstant: Self.stepLabelWidth),
            stepLabel.rightAnchor.constraint(equalTo: deleteButton.leftAnchor, constant: -10),
            
            stepButton.topAnchor.constraint(equalTo: stepLabel.topAnchor),
            stepButton.leftAnchor.constraint(equalTo: positionLabel.leftAnchor),
            stepButton.rightAnchor.constraint(equalTo: stepLabel.rightAnchor),
            stepButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -Self.spacing/2),
            
            deleteButton.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Self.spacing/2),
            deleteButton.widthAnchor.constraint(equalToConstant: Self.buttonSize),
            deleteButton.heightAnchor.constraint(equalToConstant: Self.buttonSize),
            deleteButton.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -10),
        ])
    }
    
    @objc private func stepButtonPressed() {
        buttonDelegate.stepButtonPressed(id)
    }
    
    @objc private func deleteButtonPressed() {
        buttonDelegate.deleteStepButtonPressed(id)
    }
    
}
