//
//  CreateToDoStepView.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-02-28.
//

import UIKit

class CreateToDoStepView: UIView {
    
    static let maxDimming : CGFloat = 0.7

    let dimmingView : UIView = .viewPreppedForAutoLayout()
    let modalView : UIView = .viewPreppedForAutoLayout()
    let titleLabel : UILabel = .preppedForAutoLayout()
    let stepTextView : UITextView = .textPreppedForAutoLayout()
    let cancelButton : BackgroundButton = .backgroundPreppedForAutoLayout()
    let saveButton : BackgroundButton = .backgroundPreppedForAutoLayout()
    
    unowned var buttonDelegate : CreateToDoStepButtonDelegate!
    
    var height : CGFloat {
        get { modalHeightConstraint.constant }
    }
    
    var bottomSafeArea : CGFloat = 0 {
        didSet { updateHeight() }
    }
    
    private var modalHeightConstraint : NSLayoutConstraint!
    private(set) var modalBottomConstraint : NSLayoutConstraint!
    
    private let textViewHeight : CGFloat = UIScreen.height*0.3
    private let buttonWidth = UIScreen.width*0.4
    private let buttonHeight = UIScreen.width*0.4*0.27
    private let titleFont : UIFont = .systemFont(ofSize: 14, weight: .medium)
    

    init() {
        super.init(frame: .zero)
        
        configureViews()
        setUpGestures()
        
        self.addSubview(dimmingView)
        self.addSubview(modalView)
        self.addSubview(titleLabel)
        self.addSubview(stepTextView)
        self.addSubview(cancelButton)
        self.addSubview(saveButton)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard let superview = superview else { return }
        
        pinTo(superview)
    }
    
    private func configureViews() {
        
        dimmingView.backgroundColor = .black
        dimmingView.alpha = 0
        
        modalView.backgroundColor = .white
        modalView.layer.cornerRadius = 20
        modalView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        titleLabel.font = titleFont
        titleLabel.textColor = .darkGray
        titleLabel.textAlignment = .center
        
        stepTextView.font = .systemFont(ofSize: 16)
        stepTextView.layer.borderWidth = 1
        stepTextView.layer.borderColor = UIColor.black.withAlphaComponent(0.1).cgColor
        stepTextView.layer.cornerRadius = 20
        stepTextView.textContainerInset = .init(top: 10, left: 5, bottom: 10, right: 5)
        
        let buttonFont = UIFont.systemFont(ofSize: 16, weight: .bold)
        
        cancelButton.setTitle("Cancel", for: .normal)
        cancelButton.setTitleColor(.customPurple, for: .normal)
        cancelButton.titleLabel?.font = buttonFont
        cancelButton.layer.cornerRadius = buttonHeight/2
        cancelButton.layer.borderWidth = 1
        cancelButton.layer.borderColor = UIColor.customPurple.cgColor
        cancelButton.baseColor = .white
        cancelButton.highlightedColor = .customPurple.withAlphaComponent(0.2)
        cancelButton.disabledColor = .white
        
        saveButton.setTitle("Save", for: .normal)
        saveButton.setTitleColor(.white, for: .normal)
        saveButton.titleLabel?.font = buttonFont
        saveButton.layer.cornerRadius = buttonHeight/2
        saveButton.setColorForAllStates(.customPurple)
    }
    
    private func setUpGestures() {
        cancelButton.addTarget(self, action: #selector(cancelButtonPressed), for: .touchUpInside)
        saveButton.addTarget(self, action: #selector(saveButtonPressed), for: .touchUpInside)
    }
    
    private func makeConstraints() {
        //UIScreen.width*0.2/3
        let sideSpacing = UIScreen.width*0.05
        let startingHeight = 60+textViewHeight+buttonHeight
        
        modalHeightConstraint = modalView.heightAnchor.constraint(equalToConstant: 0)
        modalBottomConstraint = modalView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0)
        dimmingView.pinTo(self)
        
        NSLayoutConstraint.activate([
            modalView.widthAnchor.constraint(equalTo: widthAnchor),
            modalView.centerXAnchor.constraint(equalTo: centerXAnchor),
            modalHeightConstraint,
            modalBottomConstraint,
            
            titleLabel.topAnchor.constraint(equalTo: modalView.topAnchor, constant: 20),
            titleLabel.centerXAnchor.constraint(equalTo: modalView.centerXAnchor),
            
            stepTextView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20),
            stepTextView.centerXAnchor.constraint(equalTo: modalView.centerXAnchor),
            stepTextView.heightAnchor.constraint(equalToConstant: textViewHeight),
            stepTextView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.8),
            
            cancelButton.topAnchor.constraint(equalTo: stepTextView.bottomAnchor, constant: 40),
            cancelButton.widthAnchor.constraint(equalToConstant: buttonWidth),
            cancelButton.heightAnchor.constraint(equalToConstant: buttonHeight),
            cancelButton.leftAnchor.constraint(equalTo: leftAnchor, constant: sideSpacing),
            
            saveButton.topAnchor.constraint(equalTo: stepTextView.bottomAnchor, constant: 40),
            saveButton.widthAnchor.constraint(equalToConstant: buttonWidth),
            saveButton.heightAnchor.constraint(equalToConstant: buttonHeight),
            saveButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -sideSpacing),
        ])
    }
    
    private func updateHeight() {
        let titleHeight = String.getLabelHeight(font: titleFont)
        modalHeightConstraint.constant = 90 + textViewHeight + buttonHeight + bottomSafeArea + titleHeight
    }
    
    @objc private func cancelButtonPressed() {
        buttonDelegate.cancelButtonPressed()
    }
    
    @objc private func saveButtonPressed() {
        buttonDelegate.saveButtonPressed()
    }
    
}
