//
//  CreateToDoView.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-01-29.
//

import UIKit

class CreateToDoView: UIView {
    
    let closeButton : IconButton = .iconPreppedForAutoLayout()
    let scrollView = CreateScrollView()
    let saveButton : BackgroundButton = .backgroundPreppedForAutoLayout()
    
    unowned var buttonDelegate : CreateButtonDelegate!
    

    init() {
        super.init(frame: .zero)
        
        setUpViews()
        setUpGestures()
        
        self.addSubview(closeButton)
        self.addSubview(scrollView)
        self.addSubview(saveButton)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard let superview = superview else { return }
        
        pinToVerticalSafeArea(superview)
    }
    
    private func setUpViews() {
        
        backgroundColor = .white
        
        closeButton.setImage(.createIcon(name: "xmark", weight: .medium), for: .normal)
        closeButton.setIconColor(.black)
        closeButton.iconContentMode = .scaleAspectFit
        
        scrollView.keyboardDismissMode = .interactive
                
        saveButton.setTitle("Save", for: .normal)
        saveButton.setTitleColor(.white, for: .normal)
        saveButton.titleLabel?.font = .systemFont(ofSize: 18, weight: .bold)
        saveButton.baseColor = .customPurple
        saveButton.highlightedColor = .customPurple.withAlphaComponent(0.7)
    
    }
    
    private func setUpGestures() {
        closeButton.addTarget(self, action: #selector(closeButtonPressed), for: .touchUpInside)
        saveButton.addTarget(self, action: #selector(saveButtonPressed), for: .touchUpInside)
    }

    private func makeConstraints() {
        
        let saveButtonWidth = UIScreen.width*0.9
        let saveButtonHeight = saveButtonWidth*0.13
        
        closeButton.pinImageView()
        saveButton.layer.cornerRadius = saveButtonHeight/2
        
        NSLayoutConstraint.activate([
            closeButton.widthAnchor.constraint(equalToConstant: 30),
            closeButton.heightAnchor.constraint(equalToConstant: 30),
            closeButton.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 10),
            closeButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -20),
            
            scrollView.topAnchor.constraint(equalTo: closeButton.bottomAnchor, constant: 15),
            scrollView.leftAnchor.constraint(equalTo: leftAnchor),
            scrollView.rightAnchor.constraint(equalTo: rightAnchor),
            scrollView.bottomAnchor.constraint(equalTo: saveButton.topAnchor, constant: -15),
            
            saveButton.widthAnchor.constraint(equalToConstant: saveButtonWidth),
            saveButton.heightAnchor.constraint(equalToConstant: saveButtonHeight),
            saveButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            saveButton.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -10),
        ])
    }
    
    @objc private func closeButtonPressed() {
        buttonDelegate.closeButtonPressed()
    }
    
    @objc private func saveButtonPressed() {
        buttonDelegate.saveButtonPressed()
    }
    
}
