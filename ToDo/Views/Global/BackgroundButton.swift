//
//  BackgroundButton.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-01-13.
//

import UIKit

class BackgroundButton: UIButton {
    
    override var isHighlighted: Bool {
        didSet {
            backgroundColor = isHighlighted ? highlightedColor : baseColor
            imageView?.isHidden = isHighlighted ? true : false
        }
    }
    
    override var isEnabled: Bool {
        didSet { backgroundColor = isEnabled ? baseColor : disabledColor }
    }
    
    var baseColor : UIColor = .white {
        didSet {
            if !isHighlighted && isEnabled { // If button is enabled and not highlighted change to base color
                backgroundColor = baseColor
            }
        }
    }
    
    var highlightedColor : UIColor = .black.withAlphaComponent(0.05) {
        didSet {
            if isHighlighted && isEnabled { // If button is highlighted change color to new highlighted color
                backgroundColor = highlightedColor
            }
        }
    }
    
    var disabledColor : UIColor = .white {
        didSet {
            if !isHighlighted && !isEnabled {   // If diabled change color to new disabled color
                backgroundColor = disabledColor
            }
        }
    }
    

    init() {
        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    
    func setColorForAllStates(_ color : UIColor) {
        baseColor = color
        highlightedColor = color.withAlphaComponent(0.6)
        disabledColor = color.withAlphaComponent(0.3)
    }
    
}
