//
//  DefaultCollectionView.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-01-10.
//

import UIKit

class DefaultCollectionView: UICollectionView {
    
    init() {
        super.init(frame: .zero, collectionViewLayout: UICollectionViewLayout())
        
        setUpViews()
    }

    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func touchesShouldCancel(in view: UIView) -> Bool {
        
        view is UIButton ? true : super.touchesShouldCancel(in: view)
    }
    
    private func setUpViews() {
        backgroundColor = .white
    }
    
}
