//
//  DefaultTableView.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-01-11.
//

import UIKit

class DefaultTableView: UITableView {
    
    init(_ style : UITableView.Style) {
        super.init(frame: .zero, style: .plain)
        
        setUpViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func touchesShouldCancel(in view: UIView) -> Bool {
        
        view is UIButton ? true : super.touchesShouldCancel(in: view)
    }
    
    private func setUpViews() {
        backgroundColor = .white
    }
    
}
