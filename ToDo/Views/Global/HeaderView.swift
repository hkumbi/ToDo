//
//  HeaderView.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-01-09.
//

import UIKit

class HeaderView: UIView {

    init() {
        super.init(frame: .zero)
        
        setUpViews()
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func setUpViews()  {
        backgroundColor = .white
    }
    
    private func makeConstraints() {
        
        let height = UIScreen.height * 0.07
        
        translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            widthAnchor.constraint(equalToConstant: UIScreen.width),
            heightAnchor.constraint(equalToConstant: height)
        ])
    }

}
