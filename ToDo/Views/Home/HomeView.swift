//
//  HomeView.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-01-09.
//

import UIKit

class HomeView: UIView {
    
    let headerView = HeaderView()
    let titleLabel : UILabel = .preppedForAutoLayout()
    let deleteButton : IconButton = .iconPreppedForAutoLayout()
    let addButton : IconButton = .iconPreppedForAutoLayout()
    let tableView : DefaultTableView = .defaultPreppedForAutoLayout(.plain)
    
    unowned var buttonDelegate : HomeButtonDelegate!
    

    init() {
        super.init(frame: .zero)
        
        setUpViews()
        setUpGestures()
        
        self.addSubview(headerView)
        self.addSubview(deleteButton)
        self.addSubview(titleLabel)
        self.addSubview(addButton)
        self.addSubview(tableView)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard let superview = superview else { return }
        
        pinToTopSafeArea(superview)
    }

    private func setUpViews() {
        
        backgroundColor = .white
        
        deleteButton.setImage(.createIcon(name: "trash"), for: .normal)
        deleteButton.setIconColor(.red)
        deleteButton.iconContentMode = .scaleAspectFit
        
        titleLabel.text = "ToDos"
        titleLabel.font = .titleFont
        titleLabel.textColor = .black
        
        tableView.alwaysBounceVertical = true
        
        addButton.setImage(.createIcon(name: "plus", weight: .medium), for: .normal)
        addButton.setIconColor(.black)
        addButton.iconContentMode = .scaleAspectFit
        
    }
    
    private func setUpGestures() {
        deleteButton.addTarget(self, action: #selector(deleteButtonPressed), for: .touchUpInside)
        addButton.addTarget(self, action: #selector(addButtonPressed), for: .touchUpInside)
    }
    
    private func makeConstraints() {
        
        let sideSpacing = UIScreen.width*0.05
        let buttonWidth : CGFloat = 30
        
        deleteButton.pinImageView()
        addButton.pinImageView()
        
        NSLayoutConstraint.activate([
            headerView.topAnchor.constraint(equalTo: topAnchor),
            headerView.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            deleteButton.leftAnchor.constraint(equalTo: headerView.leftAnchor, constant: sideSpacing),
            deleteButton.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),
            deleteButton.widthAnchor.constraint(equalToConstant: buttonWidth),
            deleteButton.heightAnchor.constraint(equalTo: deleteButton.widthAnchor),
            
            titleLabel.centerXAnchor.constraint(equalTo: headerView.centerXAnchor),
            titleLabel.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),
            
            addButton.rightAnchor.constraint(equalTo: headerView.rightAnchor, constant: -sideSpacing),
            addButton.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),
            addButton.widthAnchor.constraint(equalToConstant: buttonWidth),
            addButton.heightAnchor.constraint(equalTo: addButton.widthAnchor),
            
            tableView.topAnchor.constraint(equalTo: headerView.bottomAnchor),
            tableView.leftAnchor.constraint(equalTo: leftAnchor),
            tableView.rightAnchor.constraint(equalTo: rightAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
        ])
    }
    
    @objc private func deleteButtonPressed() {
        buttonDelegate.deleteButtonPressed()
    }
    
    @objc private func addButtonPressed() {
        buttonDelegate.addButtonPressed()
    }
    
}
