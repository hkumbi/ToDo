//
//  ToDoTableViewCell.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-01-10.
//

import UIKit

class ToDoTableViewCell: UITableViewCell {
        
    static let height = calculateHeight()
    
    private static let textSpacing : CGFloat = 2.5
    private static let titleFont = UIFont.systemFont(ofSize: 18, weight: .medium)
    private static let todoFont = UIFont.systemFont(ofSize: 16)

    private static func calculateHeight() -> CGFloat {
        
        let titleHeight = String.getLabelHeight(font: titleFont)
        let todoHeight = String.getLabelHeight(font: todoFont)
        let cellSpacing : CGFloat = 30
        
        return titleHeight + todoHeight + textSpacing + cellSpacing
    }
    
    let todoButton = BackgroundButton()
    let titleLabel : UILabel = .preppedForAutoLayout()
    let todoLabel : UILabel = .preppedForAutoLayout()
    let completedButton : UIButton = .preppedForAutoLayout()
            
    var id : UUID!
    unowned var buttonDelegate : ToDoCellDelgate!

    var isCompleted : Bool {
        get { completedButton.backgroundColor == .customPurple }
        set {
            completedButton.backgroundColor = newValue ? .customPurple : .clear
            completedButton.tintColor = newValue ? .white : .clear
        }
    }
        
    var title : String {
        get { titleLabel.text ?? "" }
        set { titleLabel.text = newValue }
    }
    
    var todo : String {
        get { todoLabel.text ?? "" }
        set { todoLabel.text = newValue }
    }
    
    private let completedButtonDiam : CGFloat = 35
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setUpViews()
        setUpGesture()

        self.contentView.addSubview(todoButton)
        self.contentView.addSubview(titleLabel)
        self.contentView.addSubview(todoLabel)
        self.contentView.addSubview(completedButton)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func setUpViews() {
        backgroundColor = .white
        
        completedButton.setImage(.createIcon(name: "checkmark", weight: .black), for: .normal)
        completedButton.layer.borderColor = UIColor.customPurple.cgColor
        completedButton.layer.borderWidth = 1
        completedButton.layer.cornerRadius = completedButtonDiam/2
        completedButton.layer.cornerCurve = .circular
        completedButton.backgroundColor = .clear
        completedButton.tintColor = .clear
        completedButton.adjustsImageWhenHighlighted = false
                
        titleLabel.font = Self.titleFont
        titleLabel.textColor = .black
        titleLabel.textAlignment = .left
        
        todoLabel.font = Self.todoFont
        todoLabel.textColor = .gray
        todoLabel.textAlignment = .left
    }
    
    private func setUpGesture() {
        
        todoButton.addTarget(self, action: #selector(todoButtonPressed), for: .touchUpInside)
        completedButton.addTarget(self, action: #selector(completedButtonPressed), for: .touchUpInside)
    }
    
    private func makeConstraints() {
        
        let sideSpacing = UIScreen.width*0.05
        
        completedButton.setImageViewConstraintsByMultiplier(width: 0.6, height: 0.6)
        todoButton.pinTo(contentView)
        
        NSLayoutConstraint.activate([
            completedButton.widthAnchor.constraint(equalToConstant: completedButtonDiam),
            completedButton.heightAnchor.constraint(equalTo: completedButton.widthAnchor),
            completedButton.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            completedButton.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: sideSpacing),
            
            titleLabel.bottomAnchor.constraint(equalTo: contentView.centerYAnchor, constant: -Self.textSpacing),
            titleLabel.leftAnchor.constraint(equalTo: completedButton.rightAnchor, constant: sideSpacing),
            titleLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -sideSpacing),
            
            todoLabel.topAnchor.constraint(equalTo: contentView.centerYAnchor, constant: Self.textSpacing),
            todoLabel.leftAnchor.constraint(equalTo: completedButton.rightAnchor, constant: sideSpacing),
            todoLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -sideSpacing),
        ])
    }
    
    @objc private func todoButtonPressed() {
        buttonDelegate.todoButtonPressed(id)
    }
    
    @objc private func completedButtonPressed(){
        buttonDelegate.completeButtonPressed(id)
    }
    
}
