//
//  StepHeaderCollectionReusableView.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-01-26.
//

import UIKit

class StepHeaderCollectionReusableView: UICollectionReusableView {
    
    static let id = "StepHeaderCollectionReusableView"
    static let size = CGSize(width: UIScreen.width, height: 50)
    
    let titleLabel : UILabel = .preppedForAutoLayout()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUpViews()
        
        self.addSubview(titleLabel)
            
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func setUpViews() {
                
        titleLabel.text = "Steps"
        titleLabel.textColor = .black
        titleLabel.textAlignment = .left
        titleLabel.font = .systemFont(ofSize: 16, weight: .bold)
    }
    
    private func makeConstraints() {
        
        NSLayoutConstraint.activate([
            titleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 20),
            titleLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
        ])
    }
}
