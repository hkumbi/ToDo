//
//  StepsCollectionViewCell.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-01-23.
//

import UIKit

class StepsCollectionViewCell: UICollectionViewCell {
    
    static let id = "StepsCollectionViewCell"
    
    private static let stepFont : UIFont = .systemFont(ofSize: 15)
    
    private static let leftSpacing : CGFloat = 40
    private static let completeButtonDiam : CGFloat = 35
    private static let stepCountLabelWidth : CGFloat = 40
    private static let rightSpacing : CGFloat = 10
    
    
    static func calculateSize(_ text : String) -> CGSize {
        
        let otherWidth = leftSpacing + completeButtonDiam + stepCountLabelWidth + rightSpacing
        let stepWidth = UIScreen.width - otherWidth
        let stepHeight = String.getLabelHeight(text: text, font: stepFont, width: stepWidth)
        let height = stepHeight > completeButtonDiam ? stepHeight : completeButtonDiam
        let spacing : CGFloat = 20
        
        return CGSize(width: UIScreen.width, height: height+spacing)
    }

    let completeButton : BackgroundButton = .backgroundPreppedForAutoLayout()
    let stepCountLabel : UILabel = .preppedForAutoLayout()
    let stepLabel : UILabel = .preppedForAutoLayout()
    
    var id : UUID!
    unowned var stepDelegate : StepDelegate!
    
    var isComplete : Bool {
        get { completeButton.baseColor == .customPurple }
        set { completeButton.baseColor = newValue ? .customPurple : .white }
    }
    
    var stepCount : String {
        get { stepCountLabel.text ?? "" }
        set { stepCountLabel.text = newValue }
    }
    
    var step : String {
        get { stepLabel.text ?? "" }
        set { stepLabel.text = newValue }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUpViews()
        setUpGestures()
        
        self.contentView.addSubview(completeButton)
        self.contentView.addSubview(stepCountLabel)
        self.contentView.addSubview(stepLabel)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func setUpViews() {
        
        backgroundColor = .white
        
        completeButton.setImage(.createIcon(name: "checkmark", weight: .black), for: .normal)
        completeButton.baseColor = .customPurple
        completeButton.highlightedColor = .customPurple.withAlphaComponent(0.3)
        completeButton.layer.cornerRadius = Self.completeButtonDiam/2
        completeButton.layer.borderWidth = 1
        completeButton.layer.borderColor = UIColor.customPurple.cgColor
        completeButton.tintColor = .white
        completeButton.adjustsImageWhenHighlighted = false
        
        stepCountLabel.font = Self.stepFont
        stepCountLabel.textColor = .darkGray
        stepCountLabel.textAlignment = .center
        
        stepLabel.font = Self.stepFont
        stepLabel.textColor = .black
        stepLabel.textAlignment = .left
        stepLabel.numberOfLines = 0
    }
    
    private func setUpGestures() {
        
        completeButton.addTarget(self, action: #selector(completeButtonPressed), for: .touchUpInside)
    }
    
    private func makeConstraints() {
        
        completeButton.setImageViewConstraintsByMultiplier(width: 0.5, height: 0.5)
        
        NSLayoutConstraint.activate([
            completeButton.widthAnchor.constraint(equalToConstant: Self.completeButtonDiam),
            completeButton.heightAnchor.constraint(equalToConstant: Self.completeButtonDiam),
            completeButton.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            completeButton.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: Self.leftSpacing),
            
            stepCountLabel.topAnchor.constraint(equalTo: completeButton.topAnchor),
            stepCountLabel.leftAnchor.constraint(equalTo: completeButton.rightAnchor),
            stepCountLabel.widthAnchor.constraint(equalToConstant: Self.stepCountLabelWidth),
            
            stepLabel.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            stepLabel.leftAnchor.constraint(equalTo: stepCountLabel.rightAnchor),
            stepLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -Self.rightSpacing),
        ])
    }
    
    @objc private func completeButtonPressed() {
        stepDelegate.stepCompleteButtonPressed(id)
    }
    
}

