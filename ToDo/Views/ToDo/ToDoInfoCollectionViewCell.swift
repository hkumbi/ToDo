//
//  ToDoInfoCollectionViewCell.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-01-26.
//

import UIKit

class ToDoInfoCollectionViewCell: UICollectionViewCell {
    
    static let id = "ToDoInfoCollectionViewCell"
    
    static private let width : CGFloat = UIScreen.width
    static private let verticalSpacing : CGFloat = 20
    static private let titleWidth : CGFloat = width*0.8
    static private let notesWidth : CGFloat = width*0.9
    static private let titleFont = UIFont.systemFont(ofSize: 18, weight: .semibold)
    static private let notesFont = UIFont.systemFont(ofSize: 15)
    
    static func calculateSize(title : String, notes : String) -> CGSize {
       
        let titleHeight = String.getLabelHeight(text: title, font: titleFont, width: Self.titleWidth)
        let notesHeight = String.getLabelHeight(text: notes, font: notesFont, width: Self.notesWidth)
        let spacing = verticalSpacing*2
        let height = titleHeight + notesHeight + spacing
        
        return CGSize(width: width, height: height)
    }
    
    let titleLabel : UILabel = .preppedForAutoLayout()
    let notesLabel : UILabel = .preppedForAutoLayout()
    let noNotesLabel : UILabel = .preppedForAutoLayout()
    
    var title : String {
        get { titleLabel.text ?? "" }
        set { titleLabel.text = newValue }
    }
    
    var notes : String {
        get { notesLabel.text ?? "" }
        set { notesLabel.text = newValue }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUpViews()
        
        self.contentView.addSubview(titleLabel)
        self.contentView.addSubview(notesLabel)
        self.contentView.addSubview(noNotesLabel)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func setUpViews() {
        
        backgroundColor = .white
        
        titleLabel.font = Self.titleFont
        titleLabel.textColor = .black
        titleLabel.textAlignment = .center
        titleLabel.numberOfLines = 0
        
        notesLabel.font = Self.notesFont
        notesLabel.textColor = .black.withAlphaComponent(0.8)
        notesLabel.textAlignment = .center
        notesLabel.numberOfLines = 0

        noNotesLabel.text = "No Notes"
        noNotesLabel.textColor = .darkGray
        noNotesLabel.textAlignment = .center
        noNotesLabel.font = .italicSystemFont(ofSize: 14)
        noNotesLabel.isHidden = true
    }
    
    private func makeConstraints() {
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor),
            titleLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            titleLabel.widthAnchor.constraint(equalToConstant: Self.titleWidth),
            
            notesLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: Self.verticalSpacing),
            notesLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            notesLabel.widthAnchor.constraint(equalToConstant: Self.notesWidth),
            
            noNotesLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: Self.verticalSpacing),
            noNotesLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
        ])
    }

    
}
