//
//  ToDoView.swift
//  ToDo
//
//  Created by Herve Kumbi on 2023-01-18.
//

import UIKit

class ToDoView: UIView {
        
    let backButton : IconButton = .iconPreppedForAutoLayout()
    let completeButton : BackgroundButton = .backgroundPreppedForAutoLayout()
    let collectionView : DefaultCollectionView = .defaultPreppedForAutoLayout()
    
    unowned var todoDelegate : ToDoDelegate!
    
    var isComplete : Bool {
        get { completeButton.baseColor == .customPurple}
        set { completeButton.baseColor = newValue ? .customPurple : .white }
    }
    
    private let completeButtonHeight : CGFloat = 35
    
    
    init() {
        super.init(frame: .zero)
        
        setUpViews()
        setUpGestures()
    
        self.addSubview(backButton)
        self.addSubview(collectionView)
        self.addSubview(completeButton)

        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard let superview = superview else { return }
        
        pinToVerticalSafeArea(superview)
    }

    private func setUpViews() {
                
        backButton.setImage(.createIcon(name: "arrow.left"), for: .normal)
        backButton.setIconColor(.black)
        backButton.iconContentMode = .scaleAspectFit
        
        collectionView.backgroundColor = .white
        
        completeButton.setImage(.createIcon(name: "checkmark", weight: .black), for: .normal)
        completeButton.tintColor = .white
        completeButton.baseColor = .customPurple
        completeButton.highlightedColor = .customPurple.withAlphaComponent(0.3)
        completeButton.layer.borderWidth = 1
        completeButton.layer.borderColor = UIColor.customPurple.cgColor
        completeButton.layer.cornerRadius = 20
        completeButton.layer.cornerCurve = .continuous
        completeButton.adjustsImageWhenHighlighted = false
    }
    
    private func setUpGestures() {
        backButton.addTarget(self, action: #selector(backButtonPressed), for: .touchUpInside)
        completeButton.addTarget(self, action: #selector(completeButtonPressed), for: .touchUpInside)
    }
    
    private func makeConstraints() {
        
        let sideSpacing = UIScreen.width*0.05
        
        backButton.pinImageView()
                
        NSLayoutConstraint.activate([
            backButton.widthAnchor.constraint(equalToConstant: 30),
            backButton.heightAnchor.constraint(equalToConstant: 30),
            backButton.leftAnchor.constraint(equalTo: leftAnchor, constant: sideSpacing),
            backButton.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            
            collectionView.topAnchor.constraint(equalTo: backButton.bottomAnchor, constant: 10),
            collectionView.leftAnchor.constraint(equalTo: leftAnchor),
            collectionView.rightAnchor.constraint(equalTo: rightAnchor),
            collectionView.bottomAnchor.constraint(equalTo: completeButton.topAnchor, constant: -20),
            
            completeButton.widthAnchor.constraint(equalToConstant: 120),
            completeButton.heightAnchor.constraint(equalToConstant: 40),
            completeButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            completeButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
        ])
    }
    
    @objc private func backButtonPressed() {
        todoDelegate.backButtonPressed()
    }
    
    @objc private func completeButtonPressed() {
        todoDelegate.completeButtonPressed()
    }
    
}
